//
// ///////////////////////////////////////////////////////////////////////////////////////
//
//
//  All rights reserved by OneBanc. Redistribution or use, is strictly prohibited
//
//
//  Copyright © 2024 OneBanc Technologies Private Ltd.  https://onebanc.ai/
//
//
// ///////////////////////////////////////////////////////////////////////////////////////
    

import SwiftUI

@main
struct ContactsDemoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
