//
// ///////////////////////////////////////////////////////////////////////////////////////
//
//
//  All rights reserved by OneBanc. Redistribution or use, is strictly prohibited
//
//
//  Copyright © 2024 OneBanc Technologies Private Ltd.  https://onebanc.ai/
//
//
// ///////////////////////////////////////////////////////////////////////////////////////


import SwiftUI
import Contacts

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundStyle(.tint)
            Text("Hello, world!")
                .onAppear{
                    Task.init {
                        await getSpecificContacts()
                    }
                }
        }
        .padding()
    }
    
    func getSpecificContacts() async {
        
        // Run this in background async
        
        // Get access to the Contacts Store
        let store = CNContactStore()
        
        // Specify which data keys we want to fetch
        let keys = [CNContactGivenNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
        
        // Search criteria
        let predicate = CNContact.predicateForContacts(matchingName: "Parth")
        
        // Call method to fetch all contacts
        do {
            let contacts = try store.unifiedContacts(matching: predicate, keysToFetch: keys)
            print(contacts)
            for smt in contacts {
                print(smt.givenName)
                for number in smt.phoneNumbers {
                    switch number.label {
                    case CNLabelPhoneNumberMobile:
                        print("- Mobile: \(number.value.stringValue)")
                    case CNLabelPhoneNumberMain:
                        print("- Main: \(number.value.stringValue)")
                    default:
                        print("- Other: \(number.value.stringValue)")
                    }
                }
            }
        }
        catch {
            print("Error")
        }
    }
    
    func fetchAllContacts() async {
        
        // Run this in background async
        
        // Get access to the Contacts Store
        let store = CNContactStore()
        
        // Specify which data keys we want to fetch
        let keys = [CNContactGivenNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
        
        // Create fetch request
        let fetchRequest = CNContactFetchRequest(keysToFetch: keys)
        
        // Call method to fetch all contacts
        do {
            try store.enumerateContacts(with: fetchRequest) { contact, result in
                print(contact)
                print(contact.givenName)
                for number in contact.phoneNumbers {
                    switch number.label {
                    case CNLabelPhoneNumberMobile:
                        print("- Mobile: \(number.value.stringValue)")
                    case CNLabelPhoneNumberMain:
                        print("- Main: \(number.value.stringValue)")
                    default:
                        print("- Other: \(number.value.stringValue)")
                    }
                }
            }
        }
        catch {
            print("Error")
        }
    }
}

#Preview {
    ContentView()
}
